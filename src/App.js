import './App.css';
import Buttons from './Components/Buttons'
import { useState, useEffect } from 'react';
import { randomColorgen } from './Functions/GRC';
import { shuffle } from 'lodash';

function App() {

const [color,setColor] = useState('');
const [answers,setAnswers] = useState([]);
const [good,setGood] = useState(false);

const redo = () =>{
  const correctAnswer =randomColorgen();
  setColor(correctAnswer);
  setAnswers(shuffle([randomColorgen() ,randomColorgen(),correctAnswer]));
}

useEffect(() => { redo() },[]);

function handelAnswer (answer) {
    if (answer === color) {
     setGood(false);
     redo();
        } else{
          setGood(true);
        }
}

return (
 <div>
 <div className="parent" >
 <div className="child" style={{backgroundColor:color}} >
 {good && <div className='ans' style={{backgroundcolor: color, color: 'red'}}>Wrong answer!</div>}
 </div>
 </div>
 <div className="box-1">
 {answers.map((answer) => (<Buttons text = {answer} onClicks={() => handelAnswer(answer)} key={answer} /> ))} 
 </div>
 </div> 
  );
}

export default App;
