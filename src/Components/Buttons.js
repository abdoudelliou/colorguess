import PropTypes from 'prop-types'
import { FcDoughnutChart } from 'react-icons/fc';


function Buttons({text,onClicks}) {



 
  return (
    
    <button className="glow-on-hover" type="button" onClick={onClicks}>{text} <FcDoughnutChart /></button>
    
  )
}

Buttons.defaultProps= {
  text: '',

  }


  Buttons.prototype = {
    text: PropTypes.string,
 
  
  }


export default Buttons